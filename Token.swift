import Foundation

struct Token: Codable {
    var date: Date = Date()
    var expiresIn: Int = 120
    var accessToken: String = UUID().uuidString
    var refreshToken: String = UUID().uuidString

    var isValid: Bool {
        let now = Date()
        let seconds = TimeInterval(expiresIn)
        return now.timeIntervalSince(date) < seconds
    }
}
