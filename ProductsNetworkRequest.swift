import Foundation

class ProductsNetworkRequest: NetworkRequest {
    let requestURL: URL?
    let parameters: [String : Any]
    var method: NetworkMethod { .get }

    init() {
        var components = URLComponents()
        components.port = 3000
        components.queryItems = []
        components.scheme = "http"
        components.host = "localhost"
        components.path = "/api/products"
        components.queryItems = []
        
        parameters = [:]
        requestURL = components.url
    }
}


