import Foundation

public enum Result<Success, Failure> {
    case success(Success)
    case failure(Failure)
}

