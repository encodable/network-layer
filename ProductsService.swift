import Foundation

public protocol ProductsRequestService: AnyObject {
    func loadProducts(completion: @escaping (Result<Data?, Error>) -> Void)
}


public final class ProductsService: ProductsRequestService {

    private let networkService: NetworkService

    public init(networkService: NetworkService) {
        self.networkService = networkService
    }

    public func loadProducts(completion: @escaping (Result<Data?, Error>) -> Void) {
        let productsNetwork = ProductsNetworkRequest()
        networkService.prepareForRequest(with: productsNetwork) { result in
            completion(result)
        }
    }
}

