import Foundation

protocol AppError: Error {
    var description: String { get }
}

enum NetworkError: AppError {
    case dataLoad
    case noConnection
    case serverError(description: String)

    var description: String {
        switch self {
        case .serverError(let description):
            return description
        case .dataLoad:
            return "Возникла ошибка при загрузке данных. Приносим свои извинения за доставленные неудобства."
        case .noConnection:
            return "Отсутствует интернет соединение"
        }
    }
}



