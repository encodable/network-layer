import Foundation
import SystemConfiguration

final class Reachability {
    private var networkReachability: SCNetworkReachability? {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len =  UInt8(MemoryLayout<Any>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachable = withUnsafePointer(to: &zeroAddress, { pointer in
            return pointer.withMemoryRebound(to: sockaddr.self, capacity: MemoryLayout<Any>.size) {
                return SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
        return defaultRouteReachable
    }

    func isConnected() -> Bool {

        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(networkReachability!, &flags) {
            return false
        }

        let isReachable: Bool = flags.contains(.reachable)
        let needConnection = flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired) != 0

        return (isReachable && !needConnection)
    }
}

