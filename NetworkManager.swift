import Foundation

public protocol NetworkService {
    func prepareForRequest(
        with request: NetworkRequest,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    )
}

public final class NetworkManager: NetworkService {

    private var token: Token?
    private var tokenIsLoading: Bool = false
    private var unsafeRequests: [UnsafeRequest] = []
    private let reachability: Reachability = Reachability()
    private let requestsQueue = DispatchQueue(label: "networkManager.requestsQueue", attributes: .concurrent)

    public init() {}

    public func prepareForRequest(
        with request: NetworkRequest,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    ) {
        if reachability.isConnected() {
            if let token = token {
                if token.isValid == false {
                    checkRefreshTokenAndFetchToken(with: request, completion)
                } else {
                    sendRequest(with: request, completion)
                }
            } else {
                safelyAddRequestAndFetchToken(with: request, completion)
            }
        } else {
            completion(.failure(NetworkError.noConnection))
        }
    }

    private func sendRequest(
        with request: NetworkRequest,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    ) {
        if let baseURL = request.requestURL {
            var urlRequest = URLRequest(url: baseURL)
            urlRequest.addValue("Application/json",forHTTPHeaderField: "Accept")
            urlRequest.addValue("Application/json",forHTTPHeaderField: "Content-Type")
            urlRequest.httpMethod = request.method.rawValue.uppercased()
            urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: request.parameters)
            let task = URLSession.shared.dataTask(with: baseURL) { [weak self] data, response, error in
                guard let httpResponse = response as? HTTPURLResponse else { return }
                switch httpResponse.statusCode {
                case 200..<300:
                    completion(.success(data))
                case 401:
                    self?.checkRefreshTokenAndFetchToken(with: request, completion)
                case -1009:
                    completion(.failure(NetworkError.noConnection))
                default:
                    if let error = error {
                        completion(.failure(error))
                    }
                }
            }
            task.resume()
        }
    }

    private func loadAndUpdateToken() {
        if tokenIsLoading == false {
            tokenIsLoading = true
            print("Token Loading")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
                self?.token = Token()
                self?.tokenIsLoading = false
                print("Token Loaded")
                self?.safelySendAllRequests()
            }
        }
    }

    private func loadAndUpdateTokenViaRefreshToken(with value: String) {
        if tokenIsLoading == false {
            tokenIsLoading = true
            print("Token Loading with Refresh Token")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
                self?.token = Token()
                self?.tokenIsLoading = false
                print("Token Loaded")
                self?.safelySendAllRequests()
            }
        }
    }

    private func checkRefreshTokenAndFetchToken(
        with request: NetworkRequest,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    ) {
        if let token = token?.refreshToken {
            safelyAddRequestAndRefreshToken(with: request, token, completion)
        } else {
            safelyAddRequestAndFetchToken(with: request, completion)
        }
    }

    private func safelyAddRequestAndFetchToken(
        with request: NetworkRequest,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    ) {
        safelyAddRequest(with: request, completion)
        loadAndUpdateToken()
    }

    private func safelyAddRequestAndRefreshToken(
        with request: NetworkRequest,
        _ token: String,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    ) {
        safelyAddRequest(with: request, completion)
        loadAndUpdateTokenViaRefreshToken(with: token)
    }

    private func safelyAddRequest(
        with request: NetworkRequest,
        _ completion: @escaping (Result<Data?, Error>) -> Void
    ) {
        requestsQueue.async(flags: .barrier) { [weak self] in
            self?.unsafeRequests.append(
                UnsafeRequest(request: request, complition: completion)
            )
        }
    }

    private func safelySendAllRequests() {
        print("Unsafe requests: \(unsafeRequests.count)")
        requestsQueue.async(flags: .barrier) { [weak self] in
            self?.unsafeRequests.forEach { unsafeRequest in
                self?.sendRequest(with: unsafeRequest.request, unsafeRequest.complition)
            }
            self?.unsafeRequests.removeAll()
        }
    }
}

