import Foundation

struct UnsafeRequest {
    let request: NetworkRequest
    let complition: (Result<Data?, Error>) -> Void
}
