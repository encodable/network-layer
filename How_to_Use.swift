import Foundation

let networkService = NetworkManager()
let productsRequestService = ProductsService(networkService: networkService)

for _ in 0..<10 {
    productsRequestService.loadProducts { result in
        print(result)
    }
}

