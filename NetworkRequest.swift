import Foundation

public enum NetworkEncoding { case json, url }
public enum NetworkMethod: String { case get, post }

public protocol NetworkRequest {
    var requestURL: URL? { get }
    var method: NetworkMethod { get }
    var parameters: [String: Any] { get }
    var headers: [String: String] { get }
}

extension NetworkRequest {

    var parameters: [String: Any] {
        return [:]
    }

    var headers: [String: String] {
        return [:]
    }
}

